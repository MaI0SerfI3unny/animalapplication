﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voice() const = 0;
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        cout << "Meow" << endl;
    }
};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        cout << "Woof" << endl;
    }
};

class Horse : public Animal
{
public:
    void Voice() const override
    {
        cout << "Igogo" << endl;
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Horse();

    for (Animal* a : animals)
        a->Voice();
}